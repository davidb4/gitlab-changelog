module gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog

go 1.13

require (
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/hashicorp/go-retryablehttp v0.6.5 // indirect
	github.com/jstemmer/go-junit-report v0.9.1
	github.com/juliangruber/go-intersect v1.0.1-0.20191008103957-64ef44ecb041
	github.com/kr/pretty v0.2.0 // indirect
	github.com/mitchellh/gox v1.0.1
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
	github.com/xanzy/go-gitlab v0.31.0
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
