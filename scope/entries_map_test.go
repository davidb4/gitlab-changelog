package scope_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/assertions"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/config"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/scope"
)

func TestNewEntriesMap(t *testing.T) {
	testScope1 := config.Scope("test scope 1")
	testScope2 := config.Scope("test scope 2")

	tests := map[string]struct {
		config        config.Configuration
		expectedError error
	}{
		"empty config": {},
		"invalid config": {
			config: config.Configuration{
				Order: config.Order{testScope1, testScope2},
				Names: config.Names{
					testScope1: "scope name",
				},
			},
			expectedError: new(scope.ErrMissingScopeName),
		},
		"valid config": {
			config: config.Configuration{
				Order: config.Order{testScope1, testScope2},
				Names: config.Names{
					testScope1: "scope name",
					testScope2: "scope name",
				},
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			em, err := scope.NewEntriesMap(tt.config.Order, tt.config.Names)

			if tt.expectedError != nil {
				assertions.ErrorIs(t, err, tt.expectedError)
				return
			}

			assert.NoError(t, err)
			assert.NotNil(t, em)
		})
	}
}

func TestEntriesMap_Add(t *testing.T) {
	testScope := config.Scope("test scope")
	testEntry := "test entry"

	tests := map[string]struct {
		config        config.Configuration
		expectedError error
	}{
		"scope is unknown": {
			expectedError: new(scope.ErrUnknownScope),
		},
		"scope is known": {
			config: config.Configuration{
				Order: config.Order{testScope},
				Names: config.Names{
					testScope: "Test scope name",
				},
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			em, err := scope.NewEntriesMap(tt.config.Order, tt.config.Names)
			require.NoError(t, err)

			err = em.Add(testScope, testEntry)

			if tt.expectedError != nil {
				assertions.ErrorIs(t, err, tt.expectedError)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestEntriesMap_ForEach(t *testing.T) {
	testScope1 := config.Scope("test scope 1")
	testScope2 := config.Scope("test scope 2")
	testScope3 := config.Scope("test scope 3")
	testScopeName1 := "test scope name 1"
	testScopeName2 := "test scope name 2"
	testScopeName3 := "test scope name 3"
	testEntry1 := "test entry 1"
	testEntry2 := "test entry 2"
	testEntry3 := "test entry 3"

	cnf := config.Configuration{
		Order: config.Order{testScope1, testScope2, testScope3},
		Names: config.Names{
			testScope1: testScopeName1,
			testScope2: testScopeName2,
			testScope3: testScopeName3,
		},
	}

	em, err := scope.NewEntriesMap(cnf.Order, cnf.Names)
	require.NoError(t, err)

	err = em.Add(testScope1, testEntry1)
	require.NoError(t, err)

	err = em.Add(testScope2, testEntry2)
	require.NoError(t, err)

	err = em.Add(testScope2, testEntry3)
	require.NoError(t, err)

	entriesMapHelper := make(map[string][]string)
	err = em.ForEach(func(entries scope.Entries) error {
		entriesMapHelper[entries.ScopeName] = entries.Entries
		return nil
	})

	assert.NoError(t, err)
	require.Len(t, entriesMapHelper[testScopeName1], 1)
	assert.Contains(t, entriesMapHelper[testScopeName1], testEntry1)
	require.Len(t, entriesMapHelper[testScopeName2], 2)
	assert.Contains(t, entriesMapHelper[testScopeName2], testEntry2)
	assert.Contains(t, entriesMapHelper[testScopeName2], testEntry3)
	assert.NotContains(t, entriesMapHelper, testScopeName3)
}

func TestEntriesMap_ForEach_ErrorPassing(t *testing.T) {
	testScope1 := config.Scope("test scope 1")
	testScopeName1 := "test scope name 1"
	testEntry1 := "test entry 1"

	cnf := config.Configuration{
		Order: config.Order{testScope1},
		Names: config.Names{
			testScope1: testScopeName1,
		},
	}

	em, err := scope.NewEntriesMap(cnf.Order, cnf.Names)
	require.NoError(t, err)

	err = em.Add(testScope1, testEntry1)
	require.NoError(t, err)

	err = em.ForEach(func(entries scope.Entries) error {
		return assert.AnError
	})

	assertions.ErrorIs(t, err, assert.AnError)
}
