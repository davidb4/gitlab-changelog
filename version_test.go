package gitlab_changelog_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	gitlab_changelog "gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog"
)

func TestVersionInfo_SimpleLine(t *testing.T) {
	assert.Equal(t, "dev (HEAD)", gitlab_changelog.Version().SimpleLine())
}

func TestVersionInfo_Extended(t *testing.T) {
	v := gitlab_changelog.Version().Extended()
	assert.Contains(t, v, "gitlab-changelog")
	assert.Contains(t, v, "Version:      dev")
	assert.Contains(t, v, "Git revision: HEAD")
	assert.Contains(t, v, "Git branch:   HEAD")
	assert.Contains(t, v, "GO version:   ")
	assert.Contains(t, v, "Built:        ")
	assert.Contains(t, v, "OS/Arch:      ")
}
