package writer_test

import (
	"bytes"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/writer"
)

func TestProxy_Write(t *testing.T) {
	buf := new(bytes.Buffer)

	w := writer.NewProxy(buf)
	_, err := fmt.Fprint(w, "test")
	assert.NoError(t, err)
	assert.Equal(t, "test", buf.String())
}

func TestProxy_Flush(t *testing.T) {
	assert.NotPanics(t, func() {
		w := writer.NewProxy(nil)
		err := w.Flush()

		assert.NoError(t, err)
	})
}
