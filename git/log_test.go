package git_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/assertions"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/commander"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/git"
)

func TestGit_Log(t *testing.T) {
	testQuery := "test..query"
	testOutput := []byte(`output
of the
log`)

	tests := map[string]struct {
		query            string
		opts             *git.LogOpts
		prepareCommander func(*testing.T) (*commander.MockCommander, commander.Factory)
		expectedOutput   []byte
		expectedError    error
	}{
		"empty query and no opts": {
			prepareCommander: func(t *testing.T) (*commander.MockCommander, commander.Factory) {
				c := new(commander.MockCommander)
				c.On("Output").
					Return(testOutput, nil).
					Once()

				factory := func(_ context.Context, command string, args ...string) commander.Commander {
					assert.Equal(t, "git", command)
					require.Len(t, args, 1)
					assert.Equal(t, "log", args[0])

					return c
				}

				return c, factory
			},
			expectedOutput: testOutput,
		},
		"provided query and no opts": {
			query: testQuery,
			prepareCommander: func(t *testing.T) (*commander.MockCommander, commander.Factory) {
				c := new(commander.MockCommander)
				c.On("Output").
					Return(testOutput, nil).
					Once()

				factory := func(_ context.Context, command string, args ...string) commander.Commander {
					assert.Equal(t, "git", command)
					require.Len(t, args, 2)
					assert.Equal(t, "log", args[0])
					assert.Contains(t, args, testQuery)

					return c
				}

				return c, factory
			},
			expectedOutput: testOutput,
		},
		"provided query and opts": {
			query: testQuery,
			opts: &git.LogOpts{
				Merges: true,
			},
			prepareCommander: func(t *testing.T) (*commander.MockCommander, commander.Factory) {
				c := new(commander.MockCommander)
				c.On("Output").
					Return(testOutput, nil).
					Once()

				factory := func(_ context.Context, command string, args ...string) commander.Commander {
					assert.Equal(t, "git", command)
					require.Len(t, args, 3)
					assert.Equal(t, "log", args[0])
					assert.Contains(t, args, testQuery)
					assert.Contains(t, args, "--merges")

					return c
				}

				return c, factory
			},
			expectedOutput: testOutput,
		},
		"error on command execution": {
			query: testQuery,
			opts: &git.LogOpts{
				Merges: true,
			},
			prepareCommander: func(t *testing.T) (*commander.MockCommander, commander.Factory) {
				c := new(commander.MockCommander)
				c.On("Output").
					Return(testOutput, assert.AnError).
					Once()
				c.On("String").
					Return("command here").
					Once()

				factory := func(_ context.Context, command string, args ...string) commander.Commander {
					assert.Equal(t, "git", command)
					require.Len(t, args, 3)
					assert.Equal(t, "log", args[0])
					assert.Contains(t, args, testQuery)
					assert.Contains(t, args, "--merges")

					return c
				}

				return c, factory
			},
			expectedError: assert.AnError,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			commanderMock, commanderFactory := tt.prepareCommander(t)
			defer commanderMock.AssertExpectations(t)

			g := git.NewWithCommanderFactory(commanderFactory)
			output, err := g.Log(tt.query, tt.opts)

			if tt.expectedError != nil {
				assertions.ErrorIs(t, err, tt.expectedError)
				return
			}

			assert.NoError(t, err)
			assert.Equal(t, tt.expectedOutput, output)
		})
	}
}
