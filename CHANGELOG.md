## v0.6.0 (2020-11-13)

### Bug fixes

- Use `git log --merges` instead of `--first-parent` to fix missing merge commits !20

## v0.5.0 (2020-09-11)

### Maintenance

- Update changelog generator configuration !18
- Update changelog generator to accept new labels !16

### Other changes

- Support full MR urls in merge commit messages !19

## v0.3.0 (2020-04-10)

### Maintenance

- Update go-gitlab and dependencies versions !13
- Pin CI jobs to gitlab-org runners !10

### Other changes

- Rely on `git ls-files` and `git diff` for checking mocks !9

## v0.2.0 (2020-03-18)

### Maintenance

- Change the release-index-gen version used by CI !8
- Fix GCS releasing scripting !7

## v0.1.0 (2020-03-06)

### Technical debt reduction

- Add GCS releasing !5
- Move configuration to a separate package !4
- Add author name !3
- Increase test coverage !2
- Force CI image rebuilt !1

