package git_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/git"
)

func TestError_Error(t *testing.T) {
	e := git.NewError("test", assert.AnError)
	assert.Contains(t, e.Error(), "executing `test` git command exited with error")
	assert.Contains(t, e.Error(), assert.AnError.Error())
}

func TestError_Unwrap(t *testing.T) {
	e := git.NewError("test", assert.AnError)
	assert.Equal(t, assert.AnError, e.Unwrap())
}

func TestError_Is(t *testing.T) {
	e := git.NewError("test", nil)
	e2 := git.NewError("test", nil)
	assert.True(t, e.Is(e2))
	assert.False(t, e.Is(errors.New("test error")))
}

func TestNew(t *testing.T) {
	g := git.New()
	assert.NotNil(t, g)
}
