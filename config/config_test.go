package config_test

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/assertions"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/config"
)

func TestLoadConfig(t *testing.T) {
	tests := map[string]struct {
		prepareFile    func(t *testing.T) string
		expectedConfig config.Configuration
		expectedError  error
	}{
		"file does not exist": {
			prepareFile: func(t *testing.T) string {
				file, err := ioutil.TempFile("", "scope-config")
				require.NoError(t, err)

				err = file.Close()
				require.NoError(t, err)

				err = os.Remove(file.Name())
				require.NoError(t, err)

				return file.Name()
			},
			expectedError: os.ErrNotExist,
		},
		"invalid file": {
			prepareFile: func(t *testing.T) string {
				file, err := ioutil.TempFile("", "scope-config")
				require.NoError(t, err)

				err = file.Close()
				require.NoError(t, err)

				err = ioutil.WriteFile(file.Name(), []byte(`default_scope: ["test a", "test b"]`), 0)
				require.NoError(t, err)

				return file.Name()
			},
			expectedError: new(config.YamlErrorWrapper),
		},
		"valid file": {
			prepareFile: func(t *testing.T) string {
				file, err := ioutil.TempFile("", "scope-config")
				require.NoError(t, err)

				err = file.Close()
				require.NoError(t, err)

				err = ioutil.WriteFile(file.Name(), []byte("default_scope: test"), 0)
				require.NoError(t, err)

				return file.Name()
			},
			expectedConfig: config.Configuration{
				DefaultScope: "test",
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			fileName := tt.prepareFile(t)

			cnf, err := config.LoadConfig(fileName)

			if tt.expectedError != nil {
				assertions.ErrorIs(t, err, tt.expectedError)
				return
			}

			assert.NoError(t, err)
			assert.Equal(t, tt.expectedConfig, cnf)
		})
	}
}
