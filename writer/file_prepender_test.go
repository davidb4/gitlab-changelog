package writer_test

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/assertions"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/writer"
)

func TestFilePrepender_Write(t *testing.T) {
	w := writer.NewFilePrepender("")
	_, err := fmt.Fprint(w, "test")
	assert.NoError(t, err)
}

func TestFilePrepender_Flush(t *testing.T) {
	testContent := `

test content
here`

	tests := map[string]struct {
		prepareFile     func(t *testing.T) string
		expectedContent string
		expectedError   error
	}{
		"file doesnt exist": {
			prepareFile: func(t *testing.T) string {
				file, err := ioutil.TempFile("", "changelog-file")
				require.NoError(t, err)

				err = file.Close()
				require.NoError(t, err)

				err = os.Remove(file.Name())
				require.NoError(t, err)

				return file.Name()
			},
			expectedContent: "test",
		},
		"file is not empty": {
			prepareFile: func(t *testing.T) string {
				file, err := ioutil.TempFile("", "changelog-file")
				require.NoError(t, err)

				err = file.Close()
				require.NoError(t, err)

				err = ioutil.WriteFile(file.Name(), []byte(testContent), 06000)
				require.NoError(t, err)

				return file.Name()
			},
			expectedContent: `test

test content
here`,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			fileName := tt.prepareFile(t)

			w := writer.NewFilePrepender(fileName)
			_, err := fmt.Fprint(w, "test")
			require.NoError(t, err)

			err = w.Flush()
			if tt.expectedError != nil {
				assertions.ErrorIs(t, err, tt.expectedError)
				return
			}

			assert.NoError(t, err)

			content, err := ioutil.ReadFile(fileName)
			require.NoError(t, err)

			assert.Equal(t, tt.expectedContent, string(content))
		})
	}
}
