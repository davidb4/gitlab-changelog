package generator_test

import (
	"testing"

	"github.com/bmizerany/assert"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/generator"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/gitlab"
)

var testMR = gitlab.MergeRequest{
	IID:    1,
	Title:  "Title",
	Labels: []string{"label 1", "label 2"},

	AuthorName:   "Author Name",
	AuthorHandle: "author_name",
}

func TestMRScopeObjectAdapter_Labels(t *testing.T) {
	object := generator.NewMRScopeObjectAdapter(testMR)
	assert.Equal(t, testMR.Labels, object.Labels())
}

func TestMRScopeObjectAdapter_Entry(t *testing.T) {
	object := generator.NewMRScopeObjectAdapter(testMR)
	assert.Equal(t, "Title !1", object.Entry())
}

func TestMRScopeObjectAdapter_IncludeAuthor(t *testing.T) {
	object := generator.NewMRScopeObjectAdapter(testMR)
	assert.Equal(t, "Title !1", object.Entry())
	object.IncludeAuthor()
	assert.Equal(t, "Title !1 (Author Name @author_name)", object.Entry())
}
