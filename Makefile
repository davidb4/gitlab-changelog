export VERSION ?= $(shell (scripts/version 2>/dev/null || echo "dev") | sed -e 's/^v//g')
export REVISION ?= $(shell git rev-parse --short HEAD || echo "unknown")
export BRANCH ?= $(shell git show-ref | grep "$(REVISION)" | grep -v HEAD | awk '{print $$2}' | sed 's|refs/remotes/origin/||' | sed 's|refs/heads/||' | sort | head -n 1)
export BUILT ?= $(shell date -u +%Y-%m-%dT%H:%M:%S%z)
export CGO_ENABLED ?= 0
export GOPATH ?= ".go/"
export BUILD_PLATFORMS ?= -osarch 'linux/amd64' -osarch 'darwin/amd64'

export goJunitReport ?= $(GOPATH)/bin/go-junit-report
MOCKERY_VERSION = 1.1.0
MOCKERY = .tmp/mockery-$(MOCKERY_VERSION)
gox ?= $(GOPATH)/bin/gox

RELEASE_INDEX_GEN_VERSION ?= master
releaseIndexGen ?= .tmp/release-index-gen-$(RELEASE_INDEX_GEN_VERSION)

PKG := $(shell go list .)
PKGs := $(shell go list ./... | grep -vE "^/vendor/")

GO_LDFLAGS := -X $(PKG).VERSION=$(VERSION) \
              -X $(PKG).REVISION=$(REVISION) \
              -X $(PKG).BRANCH=$(BRANCH) \
              -X $(PKG).BUILT=$(BUILT) \
              -s -w

.PHONY: compile
compile:
	go build \
			-o build/gitlab-changelog \
			-ldflags "$(GO_LDFLAGS)" \
			./cmd/gitlab-changelog

.PHONY: compile_all
compile_all: $(gox)
	# Building project in version $(VERSION) for $(BUILD_PLATFORMS)
	$(gox) $(BUILD_PLATFORMS) \
			-ldflags "$(GO_LDFLAGS)" \
			-output="build/gitlab-changelog-{{.OS}}-{{.Arch}}" \
			./cmd/gitlab-changelog

export testsDir = ./.tests

.PHONY: tests
tests: $(testsDir) $(goJunitReport)
	@./scripts/tests normal

.PHONY: tests_race
tests_race: $(testsDir) $(goJunitReport)
	@./scripts/tests race

.PHONY: lint
lint: OUT_FORMAT ?= colored-line-number
lint: LINT_FLAGS ?=
lint:
	@golangci-lint run ./... --out-format $(OUT_FORMAT) $(LINT_FLAGS)
$(testsDir):
	# Preparing tests output directory
	@mkdir -p $@

.PHONY: fmt
fmt:
	# Fixing project code formatting...
	@go fmt $(PKGs) | awk '{if (NF > 0) {if (NR == 1) print "Please run go fmt for:"; print "- "$$1}} END {if (NF > 0) {if (NR > 0) exit 1}}'

.PHONY: mocks
mocks: $(MOCKERY)
	# Removing existing mocks
	@find * -name "mock_*.go" -delete
	# Generating new mocks
	@$(MOCKERY) -recursive -all -inpkg -dir ./

.PHONY: check_mocks
check_mocks:
	# Checking if mocks are up-to-date
	@$(MAKE) mocks
	# Checking the differences
	@git --no-pager diff --compact-summary --exit-code -- \
		$(shell git ls-files **/mock_*.go) \
		echo "Mocks up-to-date!"

.PHONY: check_modules
check_modules:
	@git diff go.sum > /tmp/gosum-$${CI_JOB_ID}-before
	@go mod tidy
	@git diff go.sum > /tmp/gosum-$${CI_JOB_ID}-after
	@diff -U0 /tmp/gosum-$${CI_JOB_ID}-before /tmp/gosum-$${CI_JOB_ID}-after

.PHONY: prepare_ci_image
prepare_ci_image: CI_IMAGE ?= gitlab-changelog
prepare_ci_image: CI_REGISTRY ?= ""
prepare_ci_image:
	# Building the $(CI_IMAGE) image
	@docker build \
			--pull \
			--no-cache \
			--build-arg GO_VERSION=$${GO_VERSION} \
			--build-arg ALPINE_VERSION=$${ALPINE_VERSION} \
			--build-arg GCLOUD_SDK_VERSION=$${GCLOUD_SDK_VERSION} \
			-t $(CI_IMAGE) \
			-f dockerfiles/ci/Dockerfile \
			dockerfiles/ci/
ifneq ($(CI_REGISTRY),)
	# Pushing the $(CI_IMAGE) image to $(CI_REGISTRY)
	@docker login --username $${CI_REGISTRY_USER} --password $${CI_REGISTRY_PASSWORD} $(CI_REGISTRY)
	@docker push $(CI_IMAGE)
	@docker logout $(CI_REGISTRY)
else
	# No CI_REGISTRY value, skipping image push
endif

latest_stable_tag := $(shell git -c versionsort.prereleaseSuffix="-rc" tag -l "v*.*.*" --sort=-v:refname | awk '!/rc/' | head -n 1)

.PHONY: release_gcs
release_gcs: CI_COMMIT_REF_NAME ?= $(BRANCH)
release_gcs: CI_COMMIT_SHA ?= $(REVISION)
release_gcs: GCS_BUCKET ?=
release_gcs: GCS_PATH ?=
release_gcs:
	@$(MAKE) index_file
ifneq ($(GCS_BUCKET),)
	@$(MAKE) sync_gcs_release GCS_URL="gs://$(GCS_BUCKET)/$(GCS_PATH)/$(CI_COMMIT_REF_NAME)/"
ifeq ($(shell git describe --exact-match --match $(latest_stable_tag) >/dev/null 2>&1; echo $$?), 0)
	@$(MAKE) sync_gcs_release GCS_URL="gs://$(GCS_BUCKET)/$(GCS_PATH)/latest/";
endif
	@$(MAKE) release_gitlab
endif

.PHONY: sync_gcs_release
sync_gcs_release: GCS_URL ?=
sync_gcs_release:
	# Syncing with $(GCS_URL)
	@gsutil rsync build/ "$(GCS_URL)"
	@gsutil acl set -r public-read "$(GCS_URL)"

.PHONY: remove_gcs_release
remove_gcs_release: CI_COMMIT_REF_NAME ?= $(BRANCH)
remove_gcs_release: GCS_BUCKET ?=
remove_gcs_release: GCS_PATH ?=
remove_gcs_release:
ifneq ($(GCS_BUCKET),)
	@gsutil rm -r "gs://$(GCS_BUCKET)/$(GCS_PATH)/$(CI_COMMIT_REF_NAME)"
endif

.PHONY: release_gitlab
release_gitlab: export CI_COMMIT_TAG ?=
release_gitlab: export CI_PROJECT_URL ?=
release_gitlab:
ifneq ($(CI_COMMIT_TAG),)
	# Saving as GitLab release at $(CI_PROJECT_URL)/-/releases
	@./scripts/gitlab_release
endif

.PHONY: index_file
index_file: export CI_COMMIT_REF_NAME ?= $(BRANCH)
index_file: export CI_COMMIT_SHA ?= $(REVISION)
index_file: $(releaseIndexGen)
	# generating index.html file
	@$(releaseIndexGen) \
		-working-directory build/ \
		-project-version $(VERSION) \
		-project-git-ref $(CI_COMMIT_REF_NAME) \
		-project-git-revision $(CI_COMMIT_SHA) \
		-project-name "GitLab Changelog generator" \
		-project-repo-url "https://gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog" \
		-gpg-key-env GPG_KEY \
		-gpg-password-env GPG_PASSPHRASE

.PHONY: generate_changelog
generate_changelog: export CHANGELOG_RELEASE ?= $(VERSION)
generate_changelog: build/gitlab-changelog
	@build/gitlab-changelog \
		-changelog-file CHANGELOG.md \
		-config-file .gitlab/changelog.yml \
		-project-id 17192985 \
		-release $(CHANGELOG_RELEASE) \
		-starting-point-matcher "v[0-9]*.[0-9]*.[0-9]*"

build/gitlab-changelog:
	@$(MAKE) compile

$(MOCKERY): OS_TYPE ?= $(shell uname -s)
$(MOCKERY): DOWNLOAD_URL = "https://github.com/vektra/mockery/releases/download/v$(MOCKERY_VERSION)/mockery_$(MOCKERY_VERSION)_$(OS_TYPE)_x86_64.tar.gz"
$(MOCKERY):
	# Installing $(DOWNLOAD_URL) as $(MOCKERY)
	@mkdir -p $(shell dirname $(MOCKERY))
	@curl -sL "$(DOWNLOAD_URL)" | tar xz -O mockery > $(MOCKERY)
	@chmod +x "$(MOCKERY)"

$(goJunitReport):
	# Installing $(goJunitReport)
	@go get github.com/jstemmer/go-junit-report

$(gox):
	# Installing $(gox)
	@go get github.com/mitchellh/gox

$(releaseIndexGen): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(releaseIndexGen): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/release-index-generator/$(RELEASE_INDEX_GEN_VERSION)/release-index-gen-$(OS_TYPE)-amd64"
$(releaseIndexGen):
	# Installing $(DOWNLOAD_URL) as $(releaseIndexGen)
	@mkdir -p $(shell dirname $(releaseIndexGen))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(releaseIndexGen)"
	@chmod +x "$(releaseIndexGen)"
