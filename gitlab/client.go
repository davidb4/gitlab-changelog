package gitlab

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

const (
	DefaultBaseURL = "https://gitlab.com/"
	DefaultPerPage = 25
)

type ClientNotCreatedError struct {
	inner error
}

func NewClientNotCreatedError(err error) *ClientNotCreatedError {
	return &ClientNotCreatedError{inner: err}
}

func (e *ClientNotCreatedError) Error() string {
	return fmt.Sprintf("couldn't create base GitLab client: %v", e.inner)
}

func (e *ClientNotCreatedError) Unwrap() error {
	return e.inner
}

func (e *ClientNotCreatedError) Is(err error) bool {
	_, ok := err.(*ClientNotCreatedError)

	return ok
}

type APIError struct {
	inner        error
	endpointType string
}

func NewAPIError(endpointType string, err error) *APIError {
	return &APIError{
		endpointType: endpointType,
		inner:        err,
	}
}

func (e *APIError) Error() string {
	return fmt.Sprintf("error while requesting %s from API: %v", e.endpointType, e.inner)
}

func (e *APIError) Unwrap() error {
	return e.inner
}

func (e *APIError) Is(err error) bool {
	_, ok := err.(*APIError)

	return ok
}

type MergeRequest struct {
	IID    int
	Title  string
	Labels []string

	AuthorName   string
	AuthorHandle string
}

func (mr MergeRequest) Author() string {
	if mr.AuthorName == "" {
		return ""
	}

	if mr.AuthorHandle == "" {
		return mr.AuthorName
	}

	return fmt.Sprintf("%s @%s", mr.AuthorName, mr.AuthorHandle)
}

type Client interface {
	ListMergeRequests(IIDs []int, perPage int) ([]MergeRequest, error)
}

func NewClient(privateToken string, projectID string) (Client, error) {
	return NewClientWithBaseURL(privateToken, projectID, DefaultBaseURL)
}

func NewClientWithBaseURL(privateToken string, projectID string, baseURL string) (Client, error) {
	glClient, err := gitlab.NewClient(privateToken, gitlab.WithBaseURL(baseURL))
	if err != nil {
		return nil, &ClientNotCreatedError{inner: err}
	}

	adapter := &gitlabAdapter{
		projectID:  projectID,
		baseClient: glClient,
	}

	return adapter, nil
}

type gitlabAdapter struct {
	baseClient *gitlab.Client

	projectID string
}

func (a *gitlabAdapter) ListMergeRequests(IIDs []int, perPage int) ([]MergeRequest, error) {
	mrs := make([]MergeRequest, 0)

	paginator := newMRListPaginator(IIDs, perPage)

	for paginator.NextPage() {
		listMROpts := paginator.GetOpts()

		logrus.WithFields(logrus.Fields{
			"per_page": perPage,
			"page":     listMROpts.Page,
		}).Debug("Requesting MR details from GitLab")

		mergeRequests, _, err := a.baseClient.MergeRequests.ListProjectMergeRequests(a.projectID, listMROpts)
		if err != nil {
			return nil, NewAPIError("merge requests", err)
		}

		mrs = appendTransformedGitLabMergeRequests(mrs, mergeRequests)
	}

	return mrs, nil
}

func appendTransformedGitLabMergeRequests(mrs []MergeRequest, gitLabMergeRequests []*gitlab.MergeRequest) []MergeRequest {
	for _, mr := range gitLabMergeRequests {
		newMR := MergeRequest{
			IID:    mr.IID,
			Title:  mr.Title,
			Labels: mr.Labels,
		}

		if mr.Author != nil {
			newMR.AuthorName = mr.Author.Name
			newMR.AuthorHandle = mr.Author.Username
		}

		mrs = append(mrs, newMR)
	}

	return mrs
}

type mrListPaginator struct {
	iids         []int
	numberOfIIDs int

	page    int
	perPage int

	currentIIDs []int
}

func newMRListPaginator(iids []int, perPage int) *mrListPaginator {
	if perPage < 1 {
		perPage = DefaultPerPage
	}

	return &mrListPaginator{
		iids:         iids,
		numberOfIIDs: len(iids),
		page:         0,
		perPage:      perPage,
	}
}

func (p *mrListPaginator) NextPage() bool {
	p.page++

	p.currentIIDs = p.shiftIIDs()
	return len(p.currentIIDs) >= 1
}

func (p *mrListPaginator) shiftIIDs() []int {
	if len(p.iids) < 1 {
		return nil
	}

	start := (p.page - 1) * p.perPage
	if start > p.numberOfIIDs {
		return nil
	}

	end := p.page * p.perPage
	if end > p.numberOfIIDs {
		end = p.numberOfIIDs
	}

	return p.iids[start:end]
}

func (p *mrListPaginator) GetOpts() *gitlab.ListProjectMergeRequestsOptions {
	return &gitlab.ListProjectMergeRequestsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: p.perPage,
			Page:    p.page,
		},
		IIDs: p.currentIIDs,
	}
}
